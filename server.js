// server.js
// where your node app starts

// init project
var express = require('express');
var app = express();

// enable CORS (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
// so that your API is remotely testable by FCC 
var cors = require('cors');
app.use(cors({optionSuccessStatus: 200}));  // some legacy browsers choke on 204

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});


// your first API endpoint... 
app.get("/api/timestamp/:date",(req, res)=>{
    let date = String(req.params.date);
    let time = new Date(Number(date));
    if(time != "Invalid Date"){
        res.json({unix:time.getTime(), utc: time.toUTCString()});
    } else{
        time = new Date(date);
        if(time != "Invalid Date"){
            res.json({unix:time.getTime(), utc: time.toUTCString()});
        } else {
            res.json({"error" : "Invalid Date" });
        }
    }
})

app.get("/api/timestamp", function (req, res) {
  let time = new Date();
  res.json({unix:time.getTime(), utc: time.toUTCString()});
});

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
